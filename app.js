/*  const apiKey = '152d1d9e3c7c42baa51b556e94884ec2'; */
const apiKey = '2879550532ee434cae197c86f4830118';
const main = document.querySelector('main');
const sourceSelector = document.querySelector('#sourceSelector');
const defaultSource = 'techcrunch';


window.addEventListener('load', async evt => {

    updateNews();
    await updateSources();
    sourceSelector.value = defaultSource;
    sourceSelector.addEventListener('change', evt => {
        updateNews(evt.target.value);
    })


    if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register('sw.js')
            .then(registration => {
                console.log(`ServiceWorker registered on scope:`, registration.scope);
                Notification.requestPermission();
                console.log(registration.pushManager.getSubscription());
                console.log({
                    registration
                });

                registration.showNotification('Vibration Sample', {
                    body: 'Buzz! Buzz! ...your food is ready now for collection at the counter...thank you!',
                    icon: '/images/fetch-dog.jpg',
                    /* vibrate: [200, 100, 200, 100, 200, 100, 200], */
                    tag: 'vibration-sample'
                });

                registration.showNotification('Andrew\'s ServiceWorker Cookbook', {
                    body: 'your food is ready...',
                })
            })
            .catch(err => {
                console.log(`ServiceWorker registration failed:`, err);
            });

    }

});

async function updateNews(source = defaultSource) {
    const res = await fetch(`https://newsapi.org/v2/top-headlines?sources=${source}&apiKey=${apiKey}`);
    const json = await res.json();
    /* console.log({ response: res, body: res.body });
    console.log({
      respondType: res.type,
      respondStatus: res.status,
      respondStatusText: res.statusText,
      resJSON: json
    }); */

    main.innerHTML = json.articles.map(createArticle).join('\n');
}

async function updateSources() {
    const res = await fetch(`https://newsapi.org/v2/sources?apiKey=${apiKey}`);
    const json = await res.json();

    sourceSelector.innerHTML = json.sources
        .map(src => `<option value="${src.id}">${src.name}</option>`).join('\n');
}

function createArticle(article) {
    return `
  <div>
  <a href = "${article.url}" target = "_blank" style="text-decoration: none">
      <h3><large><strong>${article.title}</strong></large></h3>
      <img class="art-img" src = "${article.urlToImage}">
      <p style="margin-top: 2px"><small>${article.description}</small></p>
  </a><hr>
</div>
`;
}